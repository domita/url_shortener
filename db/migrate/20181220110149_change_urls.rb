class ChangeUrls < ActiveRecord::Migration[5.0]
  def change
    remove_column :urls, :shortened_url
    add_column :urls, :shortened_id, :string
  end
end
