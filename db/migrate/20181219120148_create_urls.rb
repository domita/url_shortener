class CreateUrls < ActiveRecord::Migration[5.0]
  def change
    create_table :urls do |t|

      t.timestamps
      t.string :original_url
      t.string :shortened_url
    end
  end
end
