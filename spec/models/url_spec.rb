require 'rails_helper'

describe Url do
  describe "#shortenize_url" do

    subject do
      Url.shortenize_url(:original_url => original_url)
    end

    context "when string" do
      let(:original_url) {"https://wiadomosci.onet.pl/kraj/jaroslaw-gowin-po-nowym-roku-premier-zabierze-glos-ws-obnizenia-podatkow/5wwrpv0"}
      it "returns string" do
        expect(subject.shortened_id.nil?).to eq(false)
      end
    end

    context "when long string" do
      let(:original_url) {"https://wiadomosci.onet.pl/kraj/jaroslaw-gowin-po-nowym-roku-premier-zabierze-glos-ws-obnizenia-podatkow/5wwrpv0"}
      it "returns 8 as string length" do
        expect(subject.shortened_id.length).to eq(8)
      end
    end

    context "when string" do
      let(:original_url) {"https://wiadomosci.onet.pl/kraj/jaroslaw-gowin-po-nowym-roku-premier-zabierze-glos-ws-obnizenia-podatkow/5wwrpv0"}
      it "returns string" do
        expect(subject.shortened_url.nil?).to eq(false)
      end
    end

    context "when long string" do
      let(:original_url) {"https://wiadomosci.onet.pl/kraj/jaroslaw-gowin-po-nowym-roku-premier-zabierze-glos-ws-obnizenia-podatkow/5wwrpv0"}
      it "returns shortened string" do
        subject.shortened_id = "abc5fef3"
        expect(subject.shortened_url).to eq("https://url-change.herokuapp.com/urls/abc5fef3")
      end
    end

  end
end