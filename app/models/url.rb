class Url < ApplicationRecord
  validates :original_url, presence: {message: "URL can't be blank."}
  validates :shortened_id, presence: true, uniqueness: true
  validates :original_url,
            format: {
              with: URI.regexp(%w[http https]),
              message: 'Please introduce valid URL.'
            }

  def self.shortenize_url(original_url)
    @url = Url.new
    @url.original_url = original_url
    shortened_id = SecureRandom.hex(4).to_s
    @url[:shortened_id] = shortened_id
    @url.save
    return @url
  end

  def host_url
    "https://url-change.herokuapp.com/urls/"
  end

  def shortened_url
   host_url + shortened_id.to_s
  end

end
