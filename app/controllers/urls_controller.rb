class UrlsController < ApplicationController  
  def new
    @url = Url.new
    @url_id = params[:url_id]
    if @url_id
      @shortened_url = Url.find(@url_id)
    end 
  end

  def show
    shortened_id = params[:id]
    url = Url.find_by_shortened_id(shortened_id)  
    redirect_to url.original_url
  end

  def create
    @url = Url.shortenize_url(create_params[:original_url])
    if @url.persisted?
      redirect_to "/urls/new?url_id=#{@url.id}"
    else
      render :new
    end
  end

  private

  def create_params
    params.require(:url).permit(:original_url)
  end

end
